var express = require('express');
var router = express.Router();
var Patient = require('../models/users/Patient.js').Patient;

router.post('/update', async function(req, res, next) {
    if(Object.keys(req.body).length > 0) {
        Patient.updatePatient(req.body, function(err, result) {
            if(!err) {
                res.send(result);
            }
            else {
              res.send(err);
            }
        });
    }
    else {
      res.send({error:'not_parameters'});
    }
});
router.get('/get', function(req, res, next) {
    if(Object.keys(req.query).length > 0)  {
        Patient.get(req.query._idPatient, function(err, result) {
            if(!err) {
              res.send(result)
            }
            else {
              res.send(err);
            }
        })
    }
    else {
      res.send({error: 'not_parameters'});
    }
});

router.post('/addMedicalData', async function(req, res, next) {
    if(Object.keys(req.body).length > 0) {
        //let patient = await Patient.addMedicalData(req.body);
        let patient = await Patient.addMedicalDataConst(req.body._idPatient);
        res.send(patient)
    }
    else {
      res.send({error: 'not_parameters'});
    }
});

module.exports = router;
